/**
 * Strange Magic.
 * It draws beautiful shape like a kaleidoscope.
 * 
 * Processing 3.5.3
 * @author @deconbatch
 * @version 0.1
 * created 0.1 2020.02.23
 */

void setup() {
  size(800, 800);
  noStroke();
  colorMode(HSB, 360, 100, 100, 100);
  noLoop();
}

void draw() {

  int   frmMax   = 3;    // draw 3 patterns
  int   boldMax  = 150;  // line boldness
  int   pointMax = 5000; // point number / line
  int   lineMax  = 3;    // line number
  float minRad   = min(width, height);
  float baseHue  = random(360.0);

  translate(width * 0.5, height * 0.5);
  for (int frmCnt = 0; frmCnt < frmMax; ++frmCnt) {

    // shape factor 1
    int radDiv01 = 3 + floor(random(2.0)) * 2; // 3 or 5
    int radDiv02 = floor(random(6.0, 10.0));
    
    baseHue += 120.0;
    noiseSeed(floor(baseHue * random(100.0)));

    blendMode(BLEND);
    background(0.0, 0.0, 90.0, 100.0);
    fill(0.0, 0.0, 100.0, 50.0);
    ellipse(0.0, 0.0, minRad/2.0, minRad/2.0);
    ellipse(0.0, 0.0, minRad/1.05, minRad/1.05);

    blendMode(DIFFERENCE);
    for (int boldCnt = 0; boldCnt < boldMax; ++boldCnt) {
      float boldRate = map(boldCnt, 0, boldMax, 0.0, 1.0);

      for (int lineCnt = 0; lineCnt < lineMax; lineCnt++) {
        // shape factor 2
        float phase01 = noise(lineCnt, boldRate);
        float phase02 = noise(lineCnt + 100.0, boldRate);

        for (int pointCnt = 0; pointCnt < pointMax; pointCnt++) {
          float pointRate = map(pointCnt, 0, pointMax, 0.0, 1.0);
          float eRad = map(lineCnt, 0, lineMax, minRad/8, minRad/2.25)
            + sin(TWO_PI * (phase01 + pointRate * radDiv01)) * 50.0
            + sin(TWO_PI * (phase02 + pointRate * radDiv02)) * 50.0;
          float eX   = eRad * cos(pointRate * TWO_PI);
          float eY   = eRad * sin(pointRate * TWO_PI);
          float eSiz = sqrt(lineCnt + 0.5) * 0.5;
          float eHue = baseHue + sin(PI * pointRate) * 150.0;

          fill(eHue % 360.0, 50.0, 5.0, 100.0);
          // draw symmetrically
          for (int i = 0; i < 4; i++) {
            rotate(HALF_PI);
            ellipse(eX, eY, eSiz, eSiz);
          }
          
        }
      }
    }
    saveFrame("frames/" + String.format("%04d", frmCnt) + ".png");
  }
  exit();
}
